<?php
/*
Plugin Name: BeLink Integration
Plugin URI: https://arclavis.com
Description: Integrate the BeLink URL-Shortener into Wordpress.
Version: 1.1
Author: Arclavis Consulting & Solutions <contact@arclavis.at>
*/

/*
 * TODO:
 * - Enable for pages and not just posts
 * - Let the user decide for a custom slug
 * - Share Icons for SM
 */

// If this file is called directly, abort.
if (!defined("WPINC")) {
    die;
}

$allowed_post_types = ["post"];

require_once dirname( __FILE__ ) .'/inc/settings.php';
require_once dirname( __FILE__ ) .'/inc/helpers.php';
require_once dirname( __FILE__ ) .'/inc/meta-box.php';
require_once dirname( __FILE__ ) .'/inc/actions.php';
require_once dirname( __FILE__ ) .'/inc/shortcodes.php';
require_once dirname( __FILE__ ) .'/inc/filters.php';