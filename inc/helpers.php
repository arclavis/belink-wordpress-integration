<?php
/**************************************************************
 Author: Arclavis Consulting & Solutions <contact@arclavis.at>
**************************************************************/

/**
 * Helper function to retrieve short URL from database.
 */
function belink_integration_get_short_url($post_id) {
	$short_url = get_option("short-url-" . $post_id);

	if ($short_url && belink_integration_check_url($short_url)) {
		return $short_url;
	}

	return false;
}

/**
 * Helper function to delete short URL from database.
 */
function belink_integration_delete_short_url($post_id) {
	if (belink_integration_get_short_url($post_id) != false) {
		delete_option("short-url-" . $post_id);
	}
}

/**
 * Check if short URL is still valid.
 */
function belink_integration_check_url($url) {
	// FIXME: This increases the counter everytime
	/*$curl = curl_init();
	curl_setopt_array($curl, [
		CURLOPT_URL => $url,
		CURLOPT_NOBODY => true,
		CURLOPT_CONNECTTIMEOUT => 10,
	]);
	curl_exec($curl);
	$code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);
	
	if ($code >= 400) {
		return false;
	}*/

	return true;
}

/**
 * Helper function to generate short URL and update database.
 */
function belink_integration_generate_short_url($post_id) {
	$post_permalink = esc_url_raw(get_the_permalink($post_id));
	$post_title = get_the_title($post_id);
	$post_status = get_post_status($post_id);

	$api_token = get_option("belink-integration-api-token", "");
	$api_url = get_option("belink-integration-api-url", "");

	// Check if short URL already exists in db.
	$short_url = belink_integration_get_short_url($post_id);

	if ($short_url != false) {
		return [true, $short_url];
	}

	// Check if connection details are set.
	if (empty($api_token) || empty($api_url)) {
		return [false, "No connection details set."];
	}

	// Check if post is published (no short URL before).
	if ($post_status != "publish") {
		return [false, "Post is not (yet) published."];
	}

	// Prepare API call.
	$api_url .= "?api_token=" . $api_token;
	$params = json_encode([
		"long_url" => $post_permalink,
		"title" => $post_title,
		"description" => "Genereated through Wordpress plugin \"BeLink Integration\" on " . get_site_url() . ".",
		"tags" => [
			"wp-integration-" . str_replace("https://", "", str_replace("http://", "", get_site_url())),
		]
	]);

	// Execute API call.
	$curl = curl_init();
	curl_setopt_array($curl, [
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_URL => $api_url,
		CURLOPT_POST => true,
		CURLOPT_POSTFIELDS => $params,
		CURLOPT_CONNECTTIMEOUT => 10,
		CURLOPT_HTTPHEADER => [
			"Content-Type: application/json",
			"Content-Length: " . strlen($params),
		],
	]);
	$response = curl_exec($curl);
	$error = curl_error($curl);
	curl_close($curl);
	
	// Check if any cURL error occured.
	if (!empty($error)) {
		return [false, "An error occured. Please check your connection settings."];
	}

	// Parse body.
	$body = json_decode($response);

	// Check if response is correct.
	if ($body == null) {
		return [false, "Something went wrong."];
	}

	if (isset($response->status) && $response->status == "error") {
		return [false, "Something went wrong: " . $response->messages];
	}

	if (!isset($body->link) || !isset($body->link->short_url)) {
		return [false, "Something went wrong."];
	}

	// Update database and return short url.
	$short_url = $body->link->short_url;
	belink_integration_delete_short_url($post_id);
	add_option("short-url-" . $post_id, $short_url);
	return [true, $short_url];
}