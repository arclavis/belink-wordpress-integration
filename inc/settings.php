<?php
/**************************************************************
 Author: Arclavis Consulting & Solutions <contact@arclavis.at>
**************************************************************/

/**
 * Add "Settings" URL next to plugin in plugin index.
 */
function belink_integration_settings_link($links) {
	$plugin_data = get_plugin_data( __FILE__ );

	$links[] = "<a href=\"" . admin_url("options-general.php?page=" . $plugin_data["TextDomain"]) . "\">Settings</a>";
	return $links;
}
add_filter("plugin_action_links_" . plugin_basename(__FILE__), "belink_integration_settings_link");

/**
 * Register settings page.
 */
function belink_integration_settings_page() {
	/* Sections */
	add_settings_section("connection_details", "Connection Details", null, "belink-integration");
	
	/* Fields */
	add_settings_field("belink-integration-api-token", "API Token", "belink_integration_api_token_render", "belink-integration", "connection_details");
	register_setting("connection_details", "belink-integration-api-token");
	
	add_settings_field("belink-integration-api-url", "API URL", "belink_integration_api_url_render", "belink-integration", "connection_details");
	register_setting("connection_details", "belink-integration-api-url");

	add_settings_field("belink-integration-add-to-bottom-post", "Add to bottom of post?", "belink_integration_add_to_bottom_post_render", "belink-integration", "connection_details");
	register_setting("connection_details", "belink-integration-add-to-bottom-post");

	add_settings_field("belink-integration-add-to-top-post", "Add to top of post?", "belink_integration_add_to_top_post_render", "belink-integration", "connection_details");
	register_setting("connection_details", "belink-integration-add-to-top-post");
}

/**
 * Render fields in settings page.
 */
function belink_integration_api_token_render() {
   ?>
	
	<input type="text" name="belink-integration-api-token" class="regular-text" value="<?php echo get_option("belink-integration-api-token"); ?>" />
	<p class="description">
		Can be found under "Account Settings" (https://your-site.com/account/settings).
	</p>

   <?php
}

function belink_integration_api_url_render() {
	?>

	<input type="text" name="belink-integration-api-url" class="regular-text" value="<?php echo get_option("belink-integration-api-url"); ?>" />
	<p class="description">
		Without the token (e.g. https://your-site.com/api/links/create).
	</p>

	<?php
}

function belink_integration_add_to_bottom_post_render() {
	?>

	<label>
		<input type="checkbox" name="belink-integration-add-to-bottom-post" class="regular-text" value="1" <?php checked('1', get_option("belink-integration-add-to-bottom-post")); ?> />
		Yes
	</label>
	<p class="description">
		Add share link to bottom of a post.
	</p>

	<?php
}

function belink_integration_add_to_top_post_render() {
	?>

	<label>
		<input type="checkbox" name="belink-integration-add-to-top-post" class="regular-text" value="1" <?php checked('1', get_option("belink-integration-add-to-top-post")); ?> />
		Yes
	</label>
	<p class="description">
		Add share link to top of a post.
	</p>

	<?php
}

add_action("admin_init", "belink_integration_settings_page");

/**
 * Render settings page.
 */
function belink_integration_render_settings_page() {
	?>

	<div class="wrap">
		<h1>URL Shortener Setting</h1>

		<form method="post" action="options.php">
			<?php
			settings_fields("connection_details");
			do_settings_sections("belink-integration");
			submit_button(); 
			?>
		</form>

		<h2>Code Integration</h2>
		<table class="form-table" role="presentation">
			<tbody>
				<tr>
					<th scope="row">Shortcode</th>
					<td>
						<input type="text" disabled class="regular-text" value="[belink-short-url]">
					</td>
				</tr>
				<tr>
					<th scope="row">PHP Code</th>
					<td>
						<input type="text" disabled class="regular-text" value="&lt;?php echo belink_integration_short_url(); ?&gt;">
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<?php
}

/**
 * Add settings page to menu.
 */
function menu_item() {
	add_submenu_page("options-general.php", "BeLink Integration", "BeLink Integration", "manage_options", "belink-integration", "belink_integration_render_settings_page");
}
 
add_action("admin_menu", "menu_item");