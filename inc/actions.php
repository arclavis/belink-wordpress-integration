<?php
/**************************************************************
 Author: Arclavis Consulting & Solutions <contact@arclavis.at>
**************************************************************/

/**
 * Post status change hook.
 */
function belink_integration_status_change_hook($new_status, $old_status, $post) {
	$post_id = $post->ID;

	if (!in_array(get_post_type($post), $allowed_post_types)) {
		return false;
	}

	if ($new_status != "publish") {
		belink_integration_delete_short_url($post_id);
	}

	if ($new_status == "publish") {
		belink_integration_generate_short_url($post_id);
	}
}

add_action("transition_post_status", "belink_integration_status_change_hook", 10, 3 );