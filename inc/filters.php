<?php
/**************************************************************
 Author: Arclavis Consulting & Solutions <contact@arclavis.at>
**************************************************************/

/**
 * Replace og:url HTML-meta-tag with short url.
 */
add_filter("wpseo_opengraph_url" , function($url) {
	$post_id = url_to_postid($url);

	$short_url = belink_integration_get_short_url($post_id);

	if ($short_url != false) {
		return $short_url;
	}

	return $url;
});

/**
 * Add short URL to top/bottom of post.
 */
function belink_integration_add_to_post($content) {
	global $allowed_post_types;

	if (!in_array(get_post_type(get_the_ID()), $allowed_post_types)) {
		return $content;
	}

	$share_text = "<p><center><strong>&#8250; Share: <a href=\"" . belink_integration_short_url() . "\">" . belink_integration_short_url() . "</a> &#8249;</strong></center></p>";
	
	if (get_option("belink-integration-add-to-top-post")) {
		$content = $share_text . $content;
	}

	if (get_option("belink-integration-add-to-bottom-post")) {
		$content .= $share_text;
	}

	return $content;
}

add_filter("the_content", "belink_integration_add_to_post");

/**
 * Remove short URL from excerpt.
 */
function belink_integration_remove_from_excerpt($content) {
	if (has_filter("the_content", "belink_integration_add_to_post")) {
		remove_filter("the_content", "belink_integration_add_to_post");
	}

	return $content;
}

add_filter("get_the_excerpt", "belink_integration_remove_from_excerpt");



/**
 * Return short URL for permalink.
 */
/*function belink_integration_get_permalink($url, $post_id, $sample, $type) {
	return $url;
}

foreach(["post", "page", "attachment", "post_type"] as $type) {
    add_filter($type . "_link", function ($url, $post_id, $sample) use ($type) {
		return apply_filters("belink-integration-get-permalink", $url, $post_id, $sample, $type);
    }, 9999, 3 );
}

add_filter("belink-integration-get-permalink", "belink_integration_get_permalink", 10, 4);*/