<?php
/**************************************************************
 Author: Arclavis Consulting & Solutions <contact@arclavis.at>
**************************************************************/

/**
 * Add meta box.
 */
function belink_integration_render_meta_box($post) {
	$post_id = $post->ID;

	// Generate short URL.
	$generate_short_url = belink_integration_generate_short_url($post_id);

	// Check if any errors occured.
	if ($generate_short_url[0] == false) {
		echo "<i>" . $generate_short_url[1] . "</i>";
		return false;
	}

	// Echo short URL.
	?>
		<p>
			<a href="<? echo $generate_short_url[1]; ?>">
				<? echo $generate_short_url[1]; ?>
			</a>
		</p>

		<p>
			Shortcode: <code>[belink-short-url]</code><br>
		</p>

		<p>
			<label>
				<input type="checkbox" name="belink-integration-delete-short-url"> Delete & Regenerate?
			</label>
		</p>
	<?php
	return true;
}

function belink_integration_meta_box() {
	add_meta_box("belink-integration-meta-box", "BeLink URL", "belink_integration_render_meta_box", "post", "side", "default");
}

add_action("add_meta_boxes", "belink_integration_meta_box");

/**
 * Delete short URL from database if checkbox selected.
 */
function belink_integration_save_post_hook($post_id) {
	if (isset($_POST["belink-integration-delete-short-url"])) {
		belink_integration_delete_short_url($post_id);
	}
}

add_action("save_post", "belink_integration_save_post_hook");