<?php
/**************************************************************
 Author: Arclavis Consulting & Solutions <contact@arclavis.at>
**************************************************************/

/**
 * Short Code.
 */
function belink_integration_short_url() {
	$short_url = belink_integration_get_short_url(get_the_ID());

	if ($short_url != false) {
		return $short_url;
	}

	return get_the_permalink();
}

add_shortcode("belink-short-url", "belink_integration_short_url");